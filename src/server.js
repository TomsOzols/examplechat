const express = require('express')
const { json, urlencoded } = require('body-parser')
const expressWs = require('express-ws')
const cookieParser = require('cookie-parser')
const { registerRoutes, registerSockets } = require('./api')
const handleErrors = require('./infrastructure/errors/handling')

const port = process.env.PORT || 44444
const { app, getWss } = expressWs(express())

app.use((request, response, next) => {
	response.header('Access-Control-Allow-Origin', '*')
  response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})
app.use(cookieParser())
app.use(urlencoded({
	extended: true,
}))
app.use(json())
app.use(handleErrors)

registerSockets(app, getWss)
registerRoutes(app)

app.listen(port, () => {
	console.log('Started server on port 44444')
})
