const { createNewUser, getUser, updateSessionKey, listAll } = require('@repositories/user')
const { AuthenticationError, ResourceExistsError } = require('@infrastructure/errors')
const { hashAMash, generatePasswordHash } = require('@utilities/cryptic')

const registerUser = async ({ userName, password }) => {
  const user = await getUser(userName)
  if (user) {
    throw ResourceExistsError('Specified user name is already taken')
  }

  const passwordHash = generatePasswordHash(password)
  const hashMash = hashAMash(userName) 
  await createNewUser(userName, passwordHash, hashMash)
  return hashMash
}

const login = async ({ userName, password }) => {
  const user = await getUser(userName)
  if (!user) {
    throw AuthenticationError('User not found')
    // throw Error('User not found')
  }

  const passwordHash = generatePasswordHash(password)
  if (user.password !== passwordHash) {
    throw AuthenticationError('User password mismatch')
    // throw Error('User password mismatch')
  }

  const hashMash = hashAMash(userName)
  await updateSessionKey(user, hashMash)
  return hashMash
}

const listAllUsers = async () => {
  return await listAll()
}

module.exports = {
  registerUser,
  listAllUsers,
  login,
}
