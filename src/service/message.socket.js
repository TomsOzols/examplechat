const { OPEN } = require('ws')

let socketEndpoints = {}

const getSocketService = (serviceName) => {
  const wss = socketEndpoints[serviceName]
  if (!wss) {
    throw Error(`No websocket connection initiated for service name: ${serviceName}`)
  }

  return wss
}

const startMessageSocket = (socketService, serviceName) => {
  const wss = socketService(serviceName)
  socketEndpoints = {
    ...socketEndpoints,
    [serviceName]: wss
  }
}

const getSocketByUserName = (wss, userName) => {
  return wss.clients.find(({ upgradeReq }) =>
    upgradeReq.cookies && upgradeReq.cookies.name === userName
  ) 
}

const broadcastToAllButSocket = (wss, message, socket, sendToSelf) => {
  const jsonResponse = JSON.stringify(message)
  wss.clients.forEach((client) => {
    if (client !== socket && client.readyState === OPEN) {
      client.send(jsonResponse)
    }
  })
  if (sendToSelf) {
    const sendToSelfJson = JSON.stringify(sendToSelf)
    socket.send(sendToSelfJson)
  }
}

const sendMessageToAllButUserName = (message, userName, serviceName, sendToSelf) => {
  const wss = getSocketService(serviceName)
  const socket = getSocketByUserName(wss, userName)
  broadcastToAllButSocket(wss, message, socket, sendToSelf)
}

const sendMessageToAllButCurrent = (message, socket, serviceName, sendToSelf) => {
  const wss = getSocketService(serviceName)
  broadcastToAllButSocket(wss, message, socket, sendToSelf)
}

module.exports = {
  startMessageSocket,
  sendMessageToAllButCurrent,
  sendMessageToAllButUserName,
}
