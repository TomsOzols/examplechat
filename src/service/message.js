let allMessages = []

const getAllMessages = () => {
	return allMessages
}

const addNewMessage = (userName, message) => {
	const newMessage = {
		userName,
		message,
	}

	allMessages = [
		...allMessages,
		newMessage,
	]
	
	return newMessage
}

module.exports = {
	getAllMessages,
	addNewMessage,
}
