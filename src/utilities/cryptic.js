// const {
//   crypto_pwhash_STRBYTES,
//   crypto_pwhash_str,
//   crypto_pwhash_OPSLIMIT_INTERACTIVE,
//   crypto_pwhash_MEMLIMIT_INTERACTIVE,
// } = require('sodium').api
const {
  randomBytes,
  createHmac,
} = require('crypto')

const hashAMash = (string) => {
  const randomized = randomBytes(16).toString('hex')
  const secret = `${string}:${randomized}`
  return createHmac('sha256', secret).digest('hex')
}

const generatePasswordHash = (password) => {
  return createHmac('sha256', password).digest('hex')
  // let hash = Buffer.from(crypto_pwhash_STRBYTES)
  // const passwordBuffer = new Buffer.from(password)
  // hash = crypto_pwhash_str(
  //   passwordBuffer,
  //   crypto_pwhash_OPSLIMIT_INTERACTIVE,
  //   crypto_pwhash_MEMLIMIT_INTERACTIVE
  // )

  // return hash
}

module.exports = {
  hashAMash,
  generatePasswordHash,
}
