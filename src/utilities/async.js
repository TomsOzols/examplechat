const wrapForExpress = (functionToWrap) => {
  return (request, response, next) => {
    functionToWrap(request, response, next).catch(next)
  }
}

module.exports = wrapForExpress
