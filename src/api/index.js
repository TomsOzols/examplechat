const { startMessageSocket } = require ('@service/message.socket')
const { serveIndex, serveContent } = require('./http/content')
const authentication = require('./http/authentication')
const message = require('./http/message')
const messageWs = require('./sockets/message')
const { MESSAGE } = require('./routeConstants')

const registerRoutes = (app) => {
	app.get('/', serveIndex)
	app.get('/content/:fileName', serveContent)
	app.use('/auth', authentication)
	app.use(MESSAGE, message)
}

const registerSockets = (app, getWss) => {
  startMessageSocket(getWss, MESSAGE)
  app.ws(`${MESSAGE}`, messageWs)
}

module.exports = {
  registerRoutes,
  registerSockets,
}
