const { addNewMessage } = require('@service/message')
const { sendMessageToAllButCurrent } = require('@service/message.socket')
const { requireWsCookie } = require('@infrastructure/cookies')

const { MESSAGE } = require('../routeConstants')

const messageWs = (socket) => {
  socket.on('open', () => requireWsCookie(socket))
  socket.on('message', (body) => {
    const hasCookie = requireWsCookie(socket)
    if (!hasCookie) {
      return
    }

    const { name } = socket.upgradeReq.cookies
    const { message } = JSON.parse(body)
    const newMessage = addNewMessage(name, message)
    const sendToSelf = {
      id: newMessage.id,
    }
    sendMessageToAllButCurrent(newMessage, socket, MESSAGE, sendToSelf)
  })
}

module.exports = messageWs
