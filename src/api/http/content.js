const serveIndex = (request, response) => {
  response.sendFile(
    'web/testClient.html', 
    {
      root: __dirname + '/../../',
    }
  )
}

const serveContent = (request, response) => {
  const fileName = request.params.fileName
  response.sendFile(
    `web/content/${fileName}`,
    {
      root: __dirname + '/../../',
    }
  )
}

module.exports = {
  serveIndex,
  serveContent,
}
