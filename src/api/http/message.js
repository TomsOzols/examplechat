const { Router } = require('express')
const { getAllMessages, addNewMessage } = require('@service/message')
const { sendMessageToAllButUserName } = require('@service/message.socket')
const { requireClientCookie } = require('@infrastructure/cookies')

const { MESSAGE } = require('../routeConstants')

const messageApi = Router()//
messageApi.use(requireClientCookie)

messageApi.get(
  '/all',
  (request, response) => {
    const messages = getAllMessages()
    response.send(messages)
  }
)

messageApi.post(
	'',
	({ body, cookies }, response) => {
		const { name } = cookies
		const { message } = body
		const newMessage = addNewMessage(name, message)
    sendMessageToAllButUserName(newMessage, name, MESSAGE)
		response.send(newMessage.id)
	}
)

module.exports = messageApi
