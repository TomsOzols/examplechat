const { Router } = require('express')
const { login, registerUser, listAllUsers } = require('@service/authentication')
const wrapForExpress = require('@utilities/async')
// const errorTypeAndStatus = [
//   { type: 'AuthenticationError', status: 401 }
// ]

const authenticationApi = Router()
authenticationApi.post(
  '/login',
  wrapForExpress(async (request, response) => {
    const { body } = request
    const expires = new Date(Date.now() + 900000)
    const sessionHash = await login(body)
    // try {
    //   sessionHash = await login(body)
    // } catch(error) {
    //   const statusCode = errorTypeAndStatus.find(({ type }) =>
    //     type === error.name
    //   ).status

    //   response.status(statusCode).send(error.message)
    // }

    response.cookie('session', sessionHash, { expires, httpOnly: false })
    response.status(204).send()
  }
))
authenticationApi.post(
  '/register',
  wrapForExpress(async (request, response) => {
    const { body } = request
    const expires = new Date(Date.now() + 900000)

    const sessionHash = await registerUser(body)
    response.cookie('session', sessionHash, { expires, httpOnly: false })
    response.status(204).send()
  }
))
authenticationApi.post(
	'/setUserCookie',
	(request, response) => {
		const { body } = request
		const expires = new Date(Date.now() + 900000)
		response.cookie('name', body.name, { expires, httpOnly: false })
		response.status(204).send()
	}
)

authenticationApi.get(
  '/listAllUsers',
  wrapForExpress(async (request, response) => {
    const users = await listAllUsers()
    response.status(200).send(users)
  }
))

module.exports = authenticationApi
