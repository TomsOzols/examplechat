/* eslint-disable */
var socket = null;

document.addEventListener('DOMContentLoaded', function(){
  openNewSocket()
})

function openNewSocket() {
  if (socket) {
    socket.onclose = function () {}
    socket.close()
  }
  
  var location = window.location
  socket = new WebSocket(`ws://${location.host}/message`)
  socket.onmessage = handleIncomingMessage
}

function getInputValue(elementId) {
	var input = document.getElementById(elementId)
	var value = input.value
	input.value = ""
	return value
}

function getWsMessageInput() {
	return getInputValue('here-be-ws-send-message')
}

function getHttpMessageInput() {
	return getInputValue('here-be-http-send-message')
}

function handleIncomingMessage(event) {
	var nodeToAdd = document.createElement('P')
	var json = JSON.parse(event.data)
	var textToAdd = document.createTextNode(json.userName + ':  ' + json.message)
	nodeToAdd.appendChild(textToAdd)

	var dataDiv = document.getElementById('here-be-data')
	dataDiv.appendChild(nodeToAdd)
}

function restartWebsocket() {
  openNewSocket()
}

function sendMessage() {
	var testMessage = {
		message: getWsMessageInput(),
	}

	socket.send(JSON.stringify(testMessage))
}

function sendMessageHttp() {
	var testMessage = {
		message: getHttpMessageInput(),
	}
	fetch(
		`/message`,
		{
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'POST',
       credentials: 'include',
			mode: 'cors',
			body: JSON.stringify(testMessage)
		}
	)
}

function setUserName() {
	var input = document.getElementById('here-be-username-input')
	fetch(
		`/auth/setUserCookie`,
		{
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'POST',
			mode: 'cors',
       credentials: 'include',
			body: JSON.stringify({
				name: input.value,
			})
		}
	)
}

function getAllMessages() {
	var messagesArea = document.getElementById('here-be-all-messages')
	fetch(
		`/message/all`,
		{
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'GET',
			credentials: 'include',
			mode: 'cors',
		}
	).then(function(response) {
		response.json().then(function(data) {
			messagesArea.innerHTML = ""
			for (var i = 0; i < data.length; i++) {
				var nodeToAdd = document.createElement('P')
				var textToAdd = document.createTextNode(data[i].userName + ":  " + data[i].message)
				nodeToAdd.appendChild(textToAdd)
				messagesArea.appendChild(nodeToAdd)
			}
		})
	})
}

function register() {
  var userName = getInputValue('here-be-register-username-input')
  var password = getInputValue('here-be-register-password-input')
  	fetch(
		`/auth/register`,
		{
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'POST',
			credentials: 'include',
			mode: 'cors',
      body: JSON.stringify({
				userName: userName,
        password: password,
			})
		},
	)
}

function listAllUsers() {
  fetch(
    '/auth/listAllUsers',
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'GET',
    }
  ).then(function(response) {
    response.json().then(function(data) {
      var userListDiv = document.getElementById('here-lay-users')
      for (var i = 0; i < data.length; i++) {
        var nodeToAdd = document.createElement('P')
        var textToAdd = document.createTextNode(data[i]['user_name'])
        nodeToAdd.appendChild(textToAdd)
        userListDiv.appendChild(nodeToAdd)
      }
    })
  })
};
