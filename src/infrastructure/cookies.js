const ERROR_RESPONSE_BODY = {
  type: 'error',
  message: 'Authentication required',
}

const cookieBaker = (request, response, next) => {
	const cookie = request.cookies.cookieName
	if (cookie === undefined) {
		const randomNumber = Math.random().toString()
		const partial = randomNumber.substring(2, randomNumber.length)
		response.cookie('auth', partial, { maxAge: 900000 })
	}

	next()
}

const requireClientCookie = (request, response, next) => {
	const { name } = request.cookies
	if (!name) {
		response
			.status(401)
			.send(ERROR_RESPONSE_BODY)
	} else {
    next()
  }
}

const requireWsCookie = (socket) => {
  const { name } = socket.upgradeReq.cookies
  if (!name) {
    socket.send(JSON.stringify(ERROR_RESPONSE_BODY))
    socket._events = null
    socket.close()
    return false
  }

  return true
}

module.exports = {
	cookieBaker,
	requireClientCookie,
  requireWsCookie,
}
