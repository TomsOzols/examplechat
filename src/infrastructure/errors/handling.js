const { AUTHENTICATION_ERROR, RESOURCE_EXISTS_ERROR } = require('./types')

const errorsAndStatuses = [
  { type: AUTHENTICATION_ERROR, status: 401 },
  { type: RESOURCE_EXISTS_ERROR, status: 409 }
]

const handleErrors = (request, response, next) => {
  try {
    next()
  } catch(error) {
    const statusCode = errorsAndStatuses.find(({ type }) =>
      type === error.name
    ).status

    if (!statusCode) {
      response.status(500).send('Something went horribly wrong. Contact Toms for directions')
    }
    
    response.status(statusCode).send(error.message)
  }
}

module.exports = handleErrors
