const { AUTHENTICATION_ERROR, RESOURCE_EXISTS_ERROR } = require('./types')
const errorMaker = require('./errorMaker')

const AuthenticationError = () => {
  errorMaker(this, arguments)
}

const ResourceExistsError = () => {
  errorMaker(this, arguments)
}

module.exports = {
  [AUTHENTICATION_ERROR]: AuthenticationError,
  [RESOURCE_EXISTS_ERROR]: ResourceExistsError,
}
