module.exports = (error, errorArguments) => {
  const temp = Error.apply(error, errorArguments)
  temp.name = error.name = error.constructor.name
  error.message = temp.message
  if (Object.defineProperty) {
    error.stack = Object.defineProperty(this, 'stack', {
      get: () => {
        return temp.stack
      },
      configurable: true,
    })
  } else {
    error.stack = temp.stack
  }

  error.prototype = Object.create(Error.prototype, {
    constructor: {
      value: error,
      writable: true,
      configurable: true,
    }
  })
}
