module.exports = {
  AUTHENTICATION_ERROR: 'AuthenticationError',
  RESOURCE_EXISTS_ERROR: 'ResourceExistsError',
}
