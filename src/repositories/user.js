const { v4 } = require('node-uuid')
const { User } = require('./models')

const createNewUser = async (user_name, password, sessionKey) => {
  const id = v4()
  return await User.create({
    id,
    user_name,
    password,
    sessionKey,
  })
}

const listAll = async () => {
  const users = await User.all()
  return users
}

const getUser = async (user_name) => {
  return await User.findOne({
    where: { user_name }
  })
}

const updateSessionKey = async (user_name, sessionKey) => {
  const user = getUser(user_name)
  if (!user) {
    throw Error('User not found')
  }

  await user.updateAttributes({
    sessionKey,
  })
}

module.exports = {
  createNewUser,
  getUser,
  listAll,
  updateSessionKey,
}
