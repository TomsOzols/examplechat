'use strict'
module.exports = function(sequelize, DataTypes) {
  var Message = sequelize.define('Message', {
    user_id: DataTypes.STRING,
    message: DataTypes.TEXT
  })

  return Message
}
