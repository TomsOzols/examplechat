module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User',
  {
    user_name: DataTypes.STRING,
    password: DataTypes.STRING,
    sessionKey: DataTypes.STRING,
  })

  User.associate = ({ Message }) => {
    User.hasMany(Message, { as: 'messages' })
  }

  return User
}
